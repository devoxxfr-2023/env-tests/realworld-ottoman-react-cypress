#!/bin/sh -x

DOMAIN=`docker run -it tutum/dnsutils dig _couchbases._tcp.$DB_HOST SRV +short  | awk '{print $4}' | rev | cut -c2- | rev`
curl -X DELETE -v -u $DB_USERNAME:$DB_PASSWORD https://$DOMAIN:18091/pools/default/buckets/$DB_BUCKET/scopes/$DB_SCOPE --insecure