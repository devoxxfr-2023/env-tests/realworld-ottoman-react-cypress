#!/bin/sh -x

DOMAIN=`dig _couchbases._tcp.$DB_HOST SRV +short  | awk '{print $4}' | rev | cut -c2- | rev`
curl -X POST -v -u $DB_USERNAME:$DB_PASSWORD https://$DOMAIN:18091/pools/default/buckets/$DB_BUCKET/scopes  -d name=$DB_SCOPE --insecure