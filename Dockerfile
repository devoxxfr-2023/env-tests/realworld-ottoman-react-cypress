FROM node:18.15-buster
COPY ./client ./client
COPY ./server ./server
RUN cd client && npm install && NODE_OPTIONS=--openssl-legacy-provider npm run build && cp -r build/* ../server/public/
RUN cd ../server && npm install
WORKDIR ./server
CMD [ "npm", "run", "start" ]    